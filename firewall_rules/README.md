firewall_rules
=========
These are the firewall rules for kubernetes, docker, and calico

Requirements
------------
Here we need ensure that we are using centos and iptables. We will disable firewalld
*NOTE* there is another playbook for firewalld rules

Role Variables
--------------
In the folder of firewall_rules/vars/ the main.yml defines to variables iptables and port.
These are the ports that will be open on the cluster and services for iptables will be enabled.

Dependencies
------------
the servers require the ports to be open for communication purposes 

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - firewall_rules 

License
-------

BSD

Author Information
------------------
EXPANSIA GROUP
