#East K8S resources

provider "aws" {
  region = "us-east-1"
}


resource "aws_instance" "consul-lead" {
  ami = var.aws_ami
  instance_type = var.instance_type 
  key_name = var.keyname
  source_dest_check = "false"
  root_block_device {
    volume_size = "50"
  }
  tags = {
    Name = "consul-lead"
    Application = "Consul-Lead"
    "Application Role" = "Consul Server"
    "Cost Center" = "Demo"
    Email = "teamtriorb@triorb.com"
    Environment = "DEV"
    OS = "RHEL_8"
    Organization = "AF DCGS, AFRL"
    Project = "CI/CD CONSUL"
 }
}
resource "aws_instance" "consul-worker" {
  ami = var.aws_ami
  instance_type = var.instance_type
  count= var.worker_count
  key_name = var.keyname
  source_dest_check = "false"
  root_block_device {
    volume_size = "50"
  }
  tags = {
    Name  = "consul-worker-${count.index + 1}"
    Application = "Consul-Workers"
    "Application Role" = "Consul Server"
    "Cost Center" = "Demo"
    Email = "teamtriorb@triorb.com"
    Environment = "DEV"
    OS = "RHEL_8"
    Organization = "AF DCGS, AFRL"
    Project = "CI/CD CONSUL"
  }
}
