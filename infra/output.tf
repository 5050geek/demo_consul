output "data-east" {
  value = var.region
}

output "ec2_public_ip" {
 value = aws_instance.consul-lead.public_ip
}

output "ec2_public_workers" {
 value = aws_instance.consul-worker.*.public_ip
}

