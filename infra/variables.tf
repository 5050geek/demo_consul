

# Global variables
variable "region" {
  default = "us-east-1"
}

variable "aws_ami" {
  default = "ami-098f16afa9edf40be"
}

variable "worker_count" {
  default= 2
}

variable "keyname" {
  default="mytest-ebs"
}

variable "pub_key" {
  default="mytest-ebs.pem"
}

variable "instance_type"{
   default = "t2.small"
}

