setup_repos
=========
Setting up kubernetes, docker, and rpm repository for packages installation


Requirements
------------
Enable the repos from amazon for install epel, kubernetes, calico, and other rpms


License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
